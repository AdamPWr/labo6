package labo6Client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class PhoneBook implements  Serializable
{

	ConcurrentMap<String,String> data;
	 

	private static final long serialVersionUID = 1L;
	PhoneBook()
	{
		data = new ConcurrentHashMap<String, String>();
	}

	
	String load(String nazwa_pliku)
	{
		try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(nazwa_pliku))){
			data =  (ConcurrentMap<String, String>) inputStream.readObject();
		} catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			return "ERROR Nie znaleziono pliku o nazwie "+nazwa_pliku;
		} catch (IOException e) 
		{
			e.printStackTrace();
			return "ERROR Blad podczas wczytywania pliku";
		}
		catch(ClassNotFoundException e)
		{
			return "ERROR Blad podczas wczytywania pliku";
		}
		
		return "OK Dane zostaly wczytane z pliku";
	}
	
	String save(String nazwa_pliku)
	{
	
		try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(nazwa_pliku)))
		{
		    outputStream.writeObject(data);
		} catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			return "ERROR Nie znaleziono pliku o nazwie "+nazwa_pliku;
		} catch (IOException e) 
		{
			e.printStackTrace();
			return "ERROR Nie udalo sie zapisac danych do pliku";
		}
		return "OK Udalo sie zapisac dane do pliku";
	}
	String get(String imie)
	{
		if(!data.containsKey(imie)) return "ERROR Nie ma takiej osoby";
		else return "OK: " + data.get(imie);
	}
	String put( String imie, String numer)
	{
		if (data.containsKey(imie))
			return "ERROR taka osoba juz istnieje";
		data.put(imie, numer);
		return "OK Dodano osobe";
	}

	synchronized String replace(String imie, String numer)
	{
		if(!data.containsKey(imie)) return "ERROR Nie ma takiej osoby";
		data.replace(imie, numer);
		return "OK Numer zostal zmieniony";
	}
	String delete(String imie)
	{
		
		if(data.remove(imie) == null)	
			return "ERROR nie ma takiej osoby";
		return "OK Usunieto osobe";
	}
	
	String list()
	{
		StringBuilder lista = new StringBuilder();
		lista.append("OK ");
		for(String imie : data.keySet())
		{
			lista.append(imie);
			lista.append(" ");
		}
		
		return lista.toString();
	}
	
}
