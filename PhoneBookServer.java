package labo6Client;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;



public class PhoneBookServer extends JFrame implements Runnable
{

	static final int SERVER_PORT = 25000;

	private static final long serialVersionUID = 1L;
	
	PhoneBook ksiazka;
	boolean nasluch = true; // dla true serwer nasluchuje polaczen od nowych klientow


	PhoneBookServer()
	{
		ksiazka =new PhoneBook();
		new Thread(this).start();
	}
	
	void closeSerwer()
	{
		nasluch = false;
	}

	@Override
	public void run() 
	{
		boolean socket_created = false;
		
		try (ServerSocket serwer = new ServerSocket(SERVER_PORT)) 
		{
			String host = InetAddress.getLocalHost().getHostName();
			System.out.println("Serwer zostal uruchomiony na hoscie " + host);
			socket_created = true;


			while (nasluch) 
			{  
				Socket socket = serwer.accept();
				if (socket != null) 
				{
					new ClientThread(this, socket);
				}
			}
		} catch (IOException e) 
		{
			System.out.println(e);
			if (!socket_created) 
			{
				JOptionPane.showMessageDialog(null, "Gniazdko dla serwera nie moze byc utworzone");
				System.exit(0);
			} else 
			{
				JOptionPane.showMessageDialog(null, "BLAD SERWERA: Nie mozna polaczyc sie z klientem ");
			}
		}
	}
		
}



