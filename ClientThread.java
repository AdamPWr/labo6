package labo6Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JOptionPane;


public class ClientThread implements Runnable
{
	
	private Socket socket;
	private String nazwa;
	private PhoneBookServer socketServer;
	
	private ObjectOutputStream outputStream = null;
	
	
	public ClientThread(PhoneBookServer phoneBookServer, Socket socket) 
	{
		socketServer =phoneBookServer ;
	  	this.socket = socket;
	  	new Thread(this).start(); 
	}
	
	

	@Override
	public void run() 
	{
		  
				String message;
			   	try( ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
			   		 ObjectInputStream input = new ObjectInputStream(socket.getInputStream()); )
			   	{
			   		outputStream = output;
			   		nazwa = (String)input.readObject();
					while(true){
						message = (String)input.readObject();
						
						String[] messageDispatched = message.split(" ");

						if(messageDispatched.length ==1)
						{
							if (message.equals("bye"))
							{
								break;
							}
							else if (message.equals("close"))
							{
								socketServer.closeSerwer();
								output.writeObject("OK closed");
							}
							else if (message.equals("list"))
							{
								output.writeObject(socketServer.ksiazka.list());
							}
							else output.writeObject("Nie rozpoznano polecenia DEBUG");
						}
						
						else if(messageDispatched.length ==2)
						{
							
							if(messageDispatched[0].equals("save"))
							{
								output.writeObject(socketServer.ksiazka.save(messageDispatched[1]));
							}
							else if(messageDispatched[0].equals("load"))
							{
								output.writeObject(socketServer.ksiazka.load(messageDispatched[1]));
							}
							else if(messageDispatched[0].equals("delete"))
							{
								output.writeObject(socketServer.ksiazka.delete(messageDispatched[1]));
								
							}
							else if(messageDispatched[0].equals("get"))
							{
								output.writeObject(socketServer.ksiazka.get(messageDispatched[1]));
							}
							else output.writeObject("Nie rozpoznano polecenia");
						}
						
						else if(messageDispatched.length ==3)
						{
							if (messageDispatched[0].equals("put"))
							{
								output.writeObject(socketServer.ksiazka.put(messageDispatched[1], messageDispatched[2]));
							}
							else if (messageDispatched[0].equals("replace"))
							{
								output.writeObject(socketServer.ksiazka.replace(messageDispatched[1], messageDispatched[2]));
							}
							else output.writeObject("Nie rozpoznano polecenia");
						}
						else output.writeObject("Error Nie rozpoznano polecenia");
						
					}
					socket.close();
					socket = null;
			   	} catch(Exception e) {
			   	}
			

	}

}
